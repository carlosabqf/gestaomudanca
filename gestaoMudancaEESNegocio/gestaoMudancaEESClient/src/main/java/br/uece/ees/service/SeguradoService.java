package br.uece.ees.service;


import br.uece.ees.entity.Segurado;


public interface SeguradoService {

	Segurado incluirSegurado(Long cpf, String nomeSegurado);
	
}
