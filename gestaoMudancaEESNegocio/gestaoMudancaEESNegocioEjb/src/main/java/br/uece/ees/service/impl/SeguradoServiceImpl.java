package br.uece.ees.service.impl;

import java.util.Random;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import br.uece.ees.entity.Segurado;
import br.uece.ees.service.SeguradoService;
import br.uece.ees.service.SeguradoServiceLocal;



@Stateless(name = "SeguradoService")
@Remote(SeguradoService.class)
@Local(SeguradoServiceLocal.class)
public class SeguradoServiceImpl implements SeguradoServiceLocal {

	public Segurado incluirSegurado(Long cpf, String nomeSegurado) {

		Segurado segurado = new Segurado();
		segurado.setCpf(cpf);
		segurado.setNome(nomeSegurado);


		Random random = new Random(cpf);
		int pseudoId = random.nextInt();
		segurado.setId((long)pseudoId);

		return segurado;
	}

}
